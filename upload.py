import json
import os
import argparse
from typing import Generator, Dict

import requests
import base64
import uuid


def encoded_data_generator(data_path: str) -> Generator[Dict[str, str]]:
    for file_name in os.listdir(data_path):
        if not file_name.startswith("."):
            with open(os.path.join(data_path, file_name), "rb") as f:
                encoded_file = base64.b64encode(f.read())
            yield {
                "id": str(uuid.uuid4()),
                "name": file_name,
                "file": encoded_file.decode()
            }


def file_upload(args: argparse.Namespace) -> None:
    """uploader function for a single document"""
    url = f"https://app.metamaze.eu/gql/api/organisations/{args.organisation_id}/projects/{args.project_id}/upload"
    default = {}
    if args.language:
        default["language"] = args.language
    if args.document_type:
        default["documentType"] = args.document_type
    options = {
        "eachFileIsADocument": bool(args.each_file_is_a_doc),
    }
    options.update({"default": default, "filter": {}})
    for file in encoded_data_generator(args.data_dir):
        request_body = {"files": [file], "options": options}
        headers = {"Content-Type": "application/json", "Authorization": f"Bearer {args.token}"}
        response = requests.request("POST", url, headers=headers, data=json.dumps(request_body))
        results = response.json()
        success = results.get("success")

        if success:
            upload_id = results.get("uploadId")
            print(f"Upload successful! UploadId: {upload_id}")
        else:
            raise Exception(f"An error occurred: {results}")
    return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Batch upload for Metamaze API.")
    parser.add_argument("--data_dir", type=str, help="Path to the directory containing the data to be uploaded")
    parser.add_argument("--token", type=str,
                        help="API token. Can be found in Project > Settings > I/O Settings > Setup Input / Output",
                        )
    parser.add_argument(
        "--organisation_id",
        help="Id of the organisation where the data should be uploaded to. In the Metamaze URL, it comes after "
             "app.metamaze.eu/",
        type=str,
    )
    parser.add_argument(
        "--project_id",
        help="Id of the project where the data should be uploaded to. In the Metamaze URL, it comes after "
             "projects/",
    )
    parser.add_argument("--language", help="Language of the documents. One of [nl-nl, fr-fr, en-us, de-de]", type=str)
    parser.add_argument("--document_type", help="The document type of the files", type=str)
    parser.add_argument("--each_file_is_a_doc", help="1 if each file is a document, 0 if it is not", type=int,
                        default=1)

    args = parser.parse_args()
    file_upload(args)
